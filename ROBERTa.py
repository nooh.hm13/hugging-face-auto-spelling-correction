from transformers import AutoTokenizer, AutoModelForMaskedLM
import torch, time

tokenizer = AutoTokenizer.from_pretrained("roberta-base")
model = AutoModelForMaskedLM.from_pretrained("roberta-base")

def correct_spelling_RoBERTa(text):
    text=str(text)
    text = text.lower()
    tokens = tokenizer.tokenize(text)
    inputs = tokenizer.encode(text, return_tensors='pt')
    outputs = model(inputs)
    predictions = torch.argmax(outputs.logits, dim=-1)
    corrected_tokens = []
    for i, token in enumerate(tokens):
       
            corrected_tokens.append(predictions[0][i].item())
    corrected_text = tokenizer.decode(corrected_tokens)
    return corrected_text.capitalize()
start_time=time.time()
print("Input: \n",essay)
print("output: ")

text = essay
corrected_text = correct_spelling_RoBERTa(text)
end_time=time.time()
total_time=start_time-end_time
print(corrected_text)
print(total_time)
