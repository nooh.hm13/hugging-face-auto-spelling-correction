import time
from transformers import pipeline


fix_spelling = pipeline("text2text-generation",model="oliverguhr/spelling-correction-english-base")
print("Input: \n",essay)
print("output: ")
start_time=time.time()
input_text = essay
corrected_text = fix_spelling (input_text,max_length=2048)
end_time=time.time()
final_time=end_time-start_time
print(corrected_text)
